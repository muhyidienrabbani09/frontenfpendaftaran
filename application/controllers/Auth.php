<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function index()
	{	// $this->load->view('auth/tes');

		$this->load->view('templateAuth/header');
		$this->load->view('auth/index');
		$this->load->view('templateAuth/footer');
	}

	public function siswa()
	{
		$this->load->view('templateAuth/header');
		$this->load->view('auth/siswa');
		$this->load->view('templateAuth/footer');
	}

	public function orangTua()
	{
		$this->load->view('templateAuth/header');
		$this->load->view('auth/orangTua');
		$this->load->view('templateAuth/footer');
	}
}
