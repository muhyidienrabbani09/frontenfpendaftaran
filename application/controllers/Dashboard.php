<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	public function index()
	{
		$this->load->view('templateDashboard/header');
		$this->load->view('templateDashboard/navbar');
		$this->load->view('templateDashboard/sidebar');
		$this->load->view('dashboard/index');
		$this->load->view('templateDashboard/footer');
	}
}
