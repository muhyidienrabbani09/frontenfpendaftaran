<nav class="navbar">
  <div class="container">
    <img src="assets/gambar/logo/lmsSiap.png" style="width: 85px;">
  </div>
</nav>
<div class="bHomePage">
  <br>

  <div class="container">
    <a href="" class="text-white" style="text-decoration: none; "> <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
    <div class="card mx-auto" style="max-width: 450px; border-radius: 20px; ">
      <div class="card-body">
        <div>
          <img src="assets/gambar/logo/logo-panjang.png" class="card-img-top p-4" style=" max-width: 70%; display: block; margin-left: auto; margin-right: auto;" alt="...">
        </div>
        <h4 class="card-title text-center"> Selamat datang di LMS-SIAP <br> Siswa</h4>
        <form>
          <div class="mb-1">
            <label for="exampleInputEmail1" class="form-label">Email</label>
            <div class="input-group ">
              <span class="input-group-text" id="basic-addon1"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
              <input type="text" class="form-control" placeholder="email.kamu@mail.co.id" aria-label="Username" aria-describedby="basic-addon1">
            </div>
          </div>
          <div class="mb-1">
            <label for="exampleInputPassword1" class="form-label">Password</label>
            <div class="input-group">
              <span class="input-group-text" id="basic-addon1"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
              <input type="text" class="form-control" placeholder="password" aria-label="Username" aria-describedby="basic-addon1">
            </div>
          </div>
          <div class="mb-3 row">
            <div class="col">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1"> <small> ingat saya </small></label>
            </div>
            <div class="col">
              <a href="">
                <p class="text-end"> <small>lupa password</small> </p>
              </a>
            </div>
          </div>
          <div class="d-grid gap-1 col-12 mx-auto">
            <a href="<?= base_url('auth/siswa') ?>" style="background: #FBC02D 0% 0% no-repeat padding-box;" class="btn b1 text-white" type="button">Masuk</a>
          </div>
        </form>
        <br>
        <p class=" text-center"> <small> atau masuk dengan</small></p>
        <div class="row">
          <div class="column">
            <img src="assets/gambar/logo/google.png" alt="Snow" style="width:20% ; display: block; margin-left: 80%; margin-right: auto; cursor:pointer;">
          </div>
          <div class="column">
            <img src="assets/gambar/logo/facebook.png" alt="Forest" style="width:20%; display: block; margin-left: auto; margin-right: auto; cursor:pointer;">
          </div>
          <div class="column">
            <img src="assets/gambar/logo/microsoft.png" alt="Mountains" style="width:20%; display: block; margin-left: auto; margin-right: 100%; cursor:pointer;">
          </div>
        </div>
        <p class=" text-center"><small> 2022 <br>LMS-SIAP BINTANG PELAJAR </small></p>
      </div>
    </div>
  </div>
  <br>