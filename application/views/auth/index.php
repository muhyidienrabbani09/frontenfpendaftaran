<nav class="navbar">
  <div class="container">
    <img src="assets/gambar/logo/lmsSiap.png" style="width: 85px;">
  </div>
</nav>
<div class="bHomePage">
  <br>
  <!-- <div class="container"> -->

  <div>
    <!-- <div class="card mb-3" style="max-width: 540px;"> -->
    <div class="card mx-auto mt-2" style="max-width: 450px;border-radius: 20px;">
      <div class="card-body">
        <img src="assets/gambar/logo/logo-kotak.png" class="card-img-top p-4" alt="...">
        <h4 class="card-title text-center" style="font: normal normal bold 25px/41px Poppins;"> Selamat datang <br> di LMS-SIAP Bintang Pelajar</h4>
        <br>
        <p class="fw-light text-center" style="font: normal normal normal 16px/25px Poppins;"><small> Untuk Siswa, silahkan log in menggunakan username dan password </small> </p>
        <p class="fw-light text-center" style="font: normal normal normal 16px/25px Poppins;"> <small> Untuk Orang tua, silahkan log in menggunakan akun Email dengan mengklik tombol Log in Orang tua Menggunakan Email yang terdaftar. </small></p>
        <br>
        <p class="fw-light text-center" style="font: normal normal normal 14px/21px Poppins;"> <small> masuk sebagai </small></p>
        <div class="row mb-2">
          <div class="col">
            <div class="d-grid gap-1 col-12 mx-auto">
              <a href="<?= base_url('Auth/siswa'); ?>" class="btn b1 text-white" type="button">Siswa</a>
            </div>
          </div>
          <div class="col">
            <div class="d-grid gap-1 col-12 mx-auto">
              <a href="auth/orangTua" class="btn b1 text-white" type="button">Orang Tua</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- </div> -->