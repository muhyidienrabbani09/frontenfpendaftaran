<!-- Layout container -->
<div class="layout-page">
    <!-- Navbar -->
    <nav style="background: #1EC28B 0% 0% no-repeat padding-box; height:50px">
        <p class="text-white mt-3 ms-4" style="font: normal normal bold 26px/42px Baloo 2;"> Learning Management System </p>
    </nav>
    <div class="row col-lg-12">
        <div class="col-lg-4">
            <h4 class="mt-4 ms-4" for=""> <strong> Assalamu'alaykum, Taufik </strong></h4>
            <p class="ms-4"> <small> Selamat datang di LMS Bintang Pelajar </small></p>
        </div>
        <div class="col-lg-5">
            <!-- Search -->
            <div class="navbar-nav p-4">
                <div class="nav-item d-flex">
                    <input type="text" style="background: #EBEDF5 0% 0% no-repeat padding-box;" class="form-control border-0 shadow-none" placeholder="mau belajar apa hari ini ?" aria-label="mau belajar apa hari ini ?" />
                </div>
            </div>
            <!-- /Search -->
        </div>
        <div class="col-lg-3">
            <!-- User -->
            <li class="nav-item navbar-dropdown dropdown-user dropdown">
                <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                    <div class="avatar avatar-online">
                        <img src="assets/dashboard/assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle" /> Taufik Sitompul S.
                    </div>
                </a>
                <ul class="dropdown-menu dropdown-menu-end">
                    <li>
                        <a class="dropdown-item" href="#">
                            <div class="d-flex">
                                <div class="flex-shrink-0 me-3">
                                    <div class="avatar avatar-online">
                                        <img src="assets/dashboard/assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle" />
                                    </div>
                                </div>
                                <div class="flex-grow-1">
                                    <span class="fw-semibold d-block">John Doe</span>
                                    <small class="text-muted">Admin</small>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <div class="dropdown-divider"></div>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">
                            <i class="bx bx-user me-2"></i>
                            <span class="align-middle">My Profile</span>
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">
                            <i class="bx bx-cog me-2"></i>
                            <span class="align-middle">Settings</span>
                        </a>
                    </li>
                    <li>
                        <a class="dropdown-item" href="#">
                            <span class="d-flex align-items-center align-middle">
                                <i class="flex-shrink-0 bx bx-credit-card me-2"></i>
                                <span class="flex-grow-1 align-middle">Billing</span>
                                <span class="flex-shrink-0 badge badge-center rounded-pill bg-danger w-px-20 h-px-20">4</span>
                            </span>
                        </a>
                    </li>
                    <li>
                        <div class="dropdown-divider"></div>
                    </li>
                    <li>
                        <a class="dropdown-item" href="auth-login-basic.html">
                            <i class="bx bx-power-off me-2"></i>
                            <span class="align-middle">Log Out</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!--/ User -->
        </div>
    </div>
    <!-- / AKHIR Navbar -->
    <div class="content-wrapper">